import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Scanner;

import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpExchange;



public class FileHandler implements HttpHandler {
String userName;
    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        System.out.println("About to read file");

        if (httpExchange.getRequestMethod() == "GET") {
            // handleGet(httpExchange);
            String response = readFile("playerscore.txt");
            httpExchange.sendResponseHeaders(200, response.getBytes().length);
            OutputStream os = httpExchange.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }

        if (httpExchange.getRequestMethod() == "POST") {
            createFile.writeFile(Primary.life, userName);
            String response = "Added new high score!";
            httpExchange.sendResponseHeaders(200, response.getBytes().length);
            OutputStream os = httpExchange.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }

    }



    private String readFile(String filename) throws IOException {
        File myFile = new File(filename);
        System.out.println("Created file readers.");
        Scanner reader = new Scanner(myFile);
        System.out.println("Reading file");

        String data = "";

        while (reader.hasNextLine()) {
            String currentLine = reader.nextLine();
            System.out.println("CURRENT LINE: " + currentLine);
            data += currentLine;
        }

        reader.close();
        System.out.println("Successfully wrote to the file.");
        return data;
    }

}
