import java.io.File;
import java.io.IOException;
import java.io.FileWriter;


public class createFile {
    public static void file() {
        try {
            File myObj = new File("playerscore.txt");

            if (myObj.createNewFile()) {
                System.out.println("File created: " + myObj.getName());
            } else {
                System.out.println("File already exists.");
            }
        } catch(IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    public static void writeFile(int life, String userName){
        try {
            FileWriter myWriter = new FileWriter("playerscore.txt");
            myWriter.write( userName + " survived the lair with " + life + " live(s).");
            myWriter.close();

            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }
}

