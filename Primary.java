import java.io.*;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;
import java.util.concurrent.TimeUnit;
import org.json.simple.JSONObject;
public class Primary {



    //adding dramatic pauses
    public static void dramaticPause3() throws InterruptedException{
        TimeUnit.SECONDS.sleep(3);
    }
    public static void dramaticPause2() throws InterruptedException{
        TimeUnit.SECONDS.sleep(2);
    }
    public static void dramaticPause1() throws InterruptedException{
        TimeUnit.SECONDS.sleep(1);
    }
    static int life;
    

    //Get the UserName
    public static String getUserName(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("\nPlease enter your character's name: ");
        String userName = scanner.next();
        return userName;
    }
    //Get the age of user's character
    public static int getAge(int age) throws InterruptedException{
        Scanner scanner = new Scanner(System.in);
        try {
            dramaticPause1();
            System.out.println("Please enter the age of your character (16-99):");
            age = scanner.nextInt();
            if (age < 16 || age > 99){
                System.out.println("Invalid entry, please enter a number between 16-99");
                getAge(age);

            } else
                return age;
        } catch (InputMismatchException exception) {
            System.out.println("Invalid entry for age, must be an integer between (16-99) please try again.");
            getAge(age);
        }
        return age;
    }

    //Intro text
    public static void intro(String userName) throws InterruptedException{


        dramaticPause2();
        System.out.println("Shall we play a game?\n");
        //Game
        dramaticPause2();
        System.out.println("In this game, you will be given 3 life tokens, hopefully this will help you complete your journey.");
        dramaticPause3();
        System.out.println("Let us Begin\n");
        dramaticPause1();
        System.out.println(".");
        dramaticPause1();
        System.out.println(".");
        dramaticPause1();
        System.out.println(".");
        dramaticPause1();
        System.out.println(".");
        System.out.println("You currently have " + life + " live(s).\n");
        dramaticPause2();
        System.out.println("You awake on the floor, \"Where am I?\" you say, " + " \"How did i get here?\"");
        dramaticPause3();
        dramaticPause1();
        System.out.println("Suddenly a growl fills the air, sending shivers down your spine. " + "\"What are you?\" you cry out, \"Who are you?\"");
        dramaticPause3();
        dramaticPause1();
        dramaticPause1();
        System.out.println(".");
        dramaticPause1();
        System.out.println(".");
        dramaticPause1();
        System.out.print("\"" + userName +  ".. welcome to my domain,\"");
        System.out.print(" a female's voice enchants the room, \"I've been expecting you.\"");
        dramaticPause3();
        System.out.println(" \"If you wish to live, you must answer these questions three.\"");
        dramaticPause1();
        System.out.println("\"Shall we begin the game that holds your life on the line?\" The Sphinx sings.\n");
        dramaticPause3();
    }

    //question and sub questions 1
    public static int questionOne(int life) throws InterruptedException {
        if(life != 0){
            dramaticPause3();
            System.out.println("What is that which in the morning goeth upon four feet; upon two feet in the afternoon; and in the Evening upon three?");
            dramaticPause1();
            System.out.println("(System: Please enter your answer):");
            Scanner scanner = new Scanner(System.in);
            String answer = scanner.nextLine();

            if (answer.equalsIgnoreCase("life") || answer.equalsIgnoreCase("ages of man") || answer.equalsIgnoreCase("life cycle") || answer.equals("human")){
                System.out.println("\"Correct\"");
                return life;
            } else
                System.out.println("Incorrect");
            life--;
            if (life != 0){
                System.out.println("You have now have " + life + " life(s) remaining.");
                dramaticPause2();
                System.out.println("\"Try another one..\"");

                System.out.println("\"I am a white box having no key or any lid. Yet there is a golden treasure inside it. What Am I ?\"");
                dramaticPause1();
                System.out.println("(System: Please enter your answer):");
                String answer2 = scanner.nextLine();
                if (answer2.equalsIgnoreCase("an egg") || answer2.equalsIgnoreCase("egg") || answer2.equalsIgnoreCase("a egg")){
                    System.out.println("\"Correct\"");
                    return life;
                } else
                    System.out.println("\"Incorrect\"");
                life--;


                if (life != 0){
                    System.out.println("You have now have " + life + " life(s) remaining.");
                    dramaticPause2();
                    System.out.println("\"Try another one..\"");
                    System.out.println("\"What gets wet while drying?\"");
                    dramaticPause1();
                    System.out.println("(System: Please enter your answer):");
                    String answer3 = scanner.nextLine();
                    if (answer3.equalsIgnoreCase("a towel") || answer3.equalsIgnoreCase("towel") || answer3.equalsIgnoreCase("towels")){
                        System.out.println("\"Correct\"");
                        return life;
                    } else
                        System.out.println("\"Incorrect\"");
                    life--;
                    return life;


                } else
                    System.out.println("\"Would you look at that you are my new meal...\" The beast unhinges it's jaws and lunges...");
                dramaticPause2();
                System.out.println("*You have been eaten... Game Over.*");
                System.exit(0);


            }else
                System.out.println("\"Would you look at that you are my new meal...\" The beast unhinges it's jaws and lunges...");
            dramaticPause2();
            System.out.println("*You have been eaten... Game Over.*");
            System.exit(0);


        } else
            System.out.println("\"Would you look at that you are my new meal...\" The beast unhinges it's jaws and lunges...");
        dramaticPause2();
        System.out.println("*You have been eaten... Game Over.*");
        System.exit(0);
        return life;
    }

    //question and sub questions 2
    public static int questionTwo(int life) throws InterruptedException{
        if (life != 0){

            System.out.println("\"Question number 2..\"");
            dramaticPause1();
            System.out.println("\"I am an odd number. Take away a letter and I become even. What number am I?\"");
            System.out.println("(System: Please enter your answer):");
            Scanner scanner = new Scanner(System.in);
            String answer = scanner.nextLine();

            if (answer.equalsIgnoreCase("seven") || answer.equals("7")){
                System.out.println("\"Correct\"");
                return life;
            } else
                System.out.println("\"Incorrect.\"");
            life--;

            if (life != 0) {
                System.out.println("System: You have now have " + life + " life(s) remaining.");
                dramaticPause2();
                System.out.println("\"Try this one out...\"");
                System.out.println("What can you break, even if you never pick it up or touch it?");
                String answer2 = scanner.nextLine();
                System.out.println("(System: Please enter your answer):");
                if (answer2.equalsIgnoreCase("a promise") || answer2.equalsIgnoreCase("promise")){
                    System.out.println("\"Correct.\"");
                    return life;
                } else
                    System.out.println("\"Incorrect.\"");
                life--;

                if (life != 0) {
                    System.out.println("System: You have now have " + life + " life(s) remaining.");
                    dramaticPause2();
                    System.out.println("\"Try this one out...\"");
                    System.out.println("\"What goes up but never comes down?\"");
                    String answer3 = scanner.nextLine();
                    System.out.println("(System: Please enter your answer):");
                    if (answer3.equalsIgnoreCase("age") || answer3.equalsIgnoreCase("my age") || answer3.equalsIgnoreCase("your age")) {
                        System.out.println("\"Correct\"");
                        return life;
                    } else
                        System.out.println("\"Incorrect\"");
                    life--;
                    return life;
                } else
                    System.out.println("\"Would you look at that you are my new meal...\" The beast unhinges it's jaws and lunges...");
                dramaticPause2();
                System.out.println("*You have been eaten... Game Over.*");
                System.exit(0);
            } else
                System.out.println("\"Would you look at that you are my new meal...\" The beast unhinges it's jaws and lunges...");
            dramaticPause2();
            System.out.println("*You have been eaten... Game Over.*");
            System.exit(0);

        }else
            System.out.println("\"Would you look at that you are my new meal...\" The beast unhinges it's jaws and lunges...");
        dramaticPause2();
        System.out.println("*You have been eaten... Game Over.*");
        System.exit(0);
        return life;
    }

    //question and sub questions 3
    public static int questionThree(int life) throws InterruptedException{
        Scanner scanner = new Scanner(System.in);
        if (life != 0 ){
            System.out.println("\"Let's see if you can get a third riddle correct..\"");
            dramaticPause2();
            System.out.println("\"Two in a corner, one in a room, zero in a house, but one in a shelter. What is it?\"");
            dramaticPause1();
            System.out.println("(System: Please enter your answer):");
            String answer = scanner.nextLine();
            if (answer.equalsIgnoreCase("the letter r") || answer.equalsIgnoreCase("r") || answer.equalsIgnoreCase("letter r")){
                System.out.println("\"Correct.\"");
                return life;
            } else
                System.out.println("\"Incorrect\"");
            life--;
            if (life != 0){
                System.out.println("\"Try this one out...\"");
                System.out.println("\"What can fill a room but takes up no space?\"");
                String answer2 = scanner.nextLine();
                System.out.println("(System: Please enter your answer):");
                if (answer2.equalsIgnoreCase("light") || answer2.equalsIgnoreCase("the light") || answer2.equalsIgnoreCase("a light")) {
                    System.out.println("\"Correct.\"");
                    return life;
                } else
                    System.out.println("\"Incorrect\"");
                life--;

                if (life !=0){
                    System.out.println("\"Last chance...\"");
                    System.out.println("\"If you drop me I’m sure to crack, but give me a smile and I’ll always smile back. What am I?\"");
                    String answer3 = scanner.nextLine();
                    System.out.println("(System: Please enter your answer):");
                    if(answer3.equalsIgnoreCase("a mirror") || answer3.equalsIgnoreCase("mirror") || answer3.equalsIgnoreCase("the mirror")){
                        System.out.println("\"Correct\"");
                        return life;
                    } else
                        life--;
                    System.out.println("\"Would you look at that you are my new meal...\" The beast unhinges it's jaws and lunges...");
                    dramaticPause2();
                    System.out.println("*You have been eaten... Game Over.*");
                    System.exit(0);

                }else
                    System.out.println("\"Would you look at that you are my new meal...\" The beast unhinges it's jaws and lunges...");
                dramaticPause2();
                System.out.println("*You have been eaten... Game Over.*");
                System.exit(0);

            } else
                System.out.println("\"Would you look at that you are my new meal...\" The beast unhinges it's jaws and lunges...");
            dramaticPause2();
            System.out.println("*You have been eaten... Game Over.*");
            System.exit(0);

        } else
            System.out.println("\"Would you look at that you are my new meal...\" The beast unhinges it's jaws and lunges...");
        dramaticPause2();
        System.out.println("*You have been eaten... Game Over.*");
        System.exit(0);
        return life;
    }

    public static String getHighscores() throws IOException {
        URL localhost = new URL("http://localhost:3000");
        System.out.println("Opening connection to: " + localhost.toString());
        HttpURLConnection connection = (HttpURLConnection) localhost.openConnection();
        connection.setRequestMethod("GET");
        int responseCode = connection.getResponseCode();
        System.out.println("Connection successful!! Response code: " + responseCode);

        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }

        in.close();

        return response.toString();
    }


    public static void main (String[] args) throws InterruptedException, IOException {


    life = 3;
        //Intro and Setup

        String title = "Lair of the Sphinx\n";
        String userName;
        int age = 0;

        System.out.println(title);
        //calls intro
       age = getAge(age);
       userName = getUserName();
       //Json display the name
    JSONObject obj = new JSONObject();

    obj.put("Name", userName);
    obj.put("Age", age);

    System.out.println("You have entered the following");
    System.out.println(obj);
    //intro
       intro(userName);

        //calls question 1
        life = questionOne(life);
        dramaticPause3();
        System.out.println("You have " + life + " life(s) remaining.");

        //calls question 2
        life = questionTwo(life);
        dramaticPause3();
        System.out.println("You have " + life + " life(s) remaining.");

        //calls question 3
        life = questionThree(life);
        dramaticPause3();

        //ending
        System.out.println("\"Congrats " + userName + " of the age of " + age + " you have answered my questions and survived my lair.\"");
        System.out.println("System: You have " + life + " life(s) remaining.");
        dramaticPause3();


    createFile.file();
    createFile.writeFile(life, userName);
    Server.start();
    String highScores = getHighscores();
    System.out.println("High scores: " + highScores);

    System.exit(0);
    }

}
